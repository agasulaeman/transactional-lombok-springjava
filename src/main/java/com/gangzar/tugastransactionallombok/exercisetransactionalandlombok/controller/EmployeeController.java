package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.controller;

import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity.Employee;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping("/v1/employee")
public class EmployeeController {

    private EmployeeService employeeService;


    @GetMapping
    public Iterable<Employee> getAll() {
        return employeeService.findAll();
    }

    @PostMapping
    public String save(@RequestBody Employee employee) {
        return employeeService.create(employee);
        //customerService.create(customer);
    }

    @PutMapping("/{id}")
    public Employee update(@PathVariable("id") String id, @PathVariable("nominal") int noRekening) {
        return employeeService.update(id, noRekening);
        //customerService.update(id, nominal);
    }

    @DeleteMapping("/{id}")
    public void removedDataEmployee(@PathVariable("id") String id) {
        employeeService.delete(id);
    }


}
