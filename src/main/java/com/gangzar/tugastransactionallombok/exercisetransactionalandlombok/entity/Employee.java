package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "m_employee")
public class Employee {

    @Id
    @Column(name = "id",nullable = false,length = 50)
    private String id;

    @Column(name = "nama",nullable = false,length = 100)
    private String nama;

    @Column(name = "tanggal_lahir",nullable = false,length = 50)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date tanggalLahir;

    @Column(name = "salary",nullable = false,length = 100)
    private double salary;

    @Column(name = "no_rekening",nullable = false,length = 50)
    private int noRekening;

    @Column(name = "bank_account",nullable = false,length = 100)
    private String bankAccount;

    @Transient
    private String education;
}
