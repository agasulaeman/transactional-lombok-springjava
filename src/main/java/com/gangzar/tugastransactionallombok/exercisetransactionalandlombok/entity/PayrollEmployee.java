package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "payroll_info")
@Builder
public class PayrollEmployee {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String employeeUuid;

    //  @Column(name = "salary",nullable = false)
    //  private double salary;

    @Column(name = "no_rekening", nullable = false)
    private int noRekening;

    //@Column(name = "bank_account",nullable = false)
    //private String bankAccount;


    @ManyToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "id")
    private Employee employee;

}
