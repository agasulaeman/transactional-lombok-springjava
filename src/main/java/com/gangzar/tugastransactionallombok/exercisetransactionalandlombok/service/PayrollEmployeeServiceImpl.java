package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.service;

import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity.Employee;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity.PayrollEmployee;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.exception.BusinessException;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.repository.PayrollEmployeeRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PayrollEmployeeServiceImpl implements PayrollEmployeeService {

    private PayrollEmployeeRepository payrollEmployeeRepository;

    public PayrollEmployeeServiceImpl(PayrollEmployeeRepository payrollEmployeeRepository) {
        this.payrollEmployeeRepository = payrollEmployeeRepository;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public PayrollEmployee save(Employee employee, int noRekening) throws BusinessException {
        PayrollEmployee payrollEmployeeBuild = PayrollEmployee.builder().noRekening(noRekening).employee(employee).build();
        PayrollEmployee payrollEmployeeSave =payrollEmployeeRepository.save(payrollEmployeeBuild);
        return payrollEmployeeSave;
    }
}
