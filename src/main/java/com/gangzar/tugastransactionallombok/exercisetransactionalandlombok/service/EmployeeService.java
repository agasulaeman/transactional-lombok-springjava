package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.service;

import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.dto.EmployeeRequest;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.dto.EmployeeResponse;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity.Employee;

import java.util.Optional;

public interface EmployeeService {
    public String create (Employee employee);
    EmployeeResponse saveData(EmployeeRequest request);
    public Optional<Employee> findByEmployeeID(String id);
    Iterable<Employee> findAll();
    void delete(String id);
    public Employee update (String id,int noRekekning);
}
