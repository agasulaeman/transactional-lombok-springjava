package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.service;

import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.dto.EmployeeRequest;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.dto.EmployeeResponse;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity.Employee;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity.PayrollEmployee;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.exception.BusinessException;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;


    private PayrollEmployeeServiceImpl payrollEmployeeServiceImpl;

    @Override
    public String create(Employee employee) {
        Employee employeeSave = employeeRepository.save(employee);
        log.info("employee save {} {} ", employeeSave, "berhasil");
        PayrollEmployee payrollEmployee = payrollEmployeeServiceImpl.save(employeeSave, employeeSave.getNoRekening());
        log.info("saving payroll employee no rekening  {} ", payrollEmployee);
        return "sukses";
    }

    @Transactional
    @Override
    public EmployeeResponse saveData(EmployeeRequest request) {
        Employee employeeSave = new Employee();
        employeeSave.setId(request.getId());
        employeeSave.setNama(request.getNama());
        employeeSave.setSalary(request.getSalary());
        employeeSave.setTanggalLahir(request.getTanggalLahir());
        employeeSave.setBankAccount(request.getBankAccount());
        employeeSave.setNoRekening(request.getNoRekening());

        Employee savedEmployee = employeeRepository.save(employeeSave);

        EmployeeResponse employeeResponse = new EmployeeResponse();
        employeeResponse.setId(savedEmployee.getId());
        employeeResponse.setNama(savedEmployee.getNama());
        employeeResponse.setSalary(savedEmployee.getSalary());
        employeeResponse.setTanggalLahir(savedEmployee.getTanggalLahir());
        employeeResponse.setBankAccount(savedEmployee.getBankAccount());
        employeeResponse.setNoRekening(savedEmployee.getNoRekening());

        return employeeResponse;
    }

    @Override
    public Optional<Employee> findByEmployeeID(String id) throws BusinessException {
        if (id.isEmpty()){
            throw new BusinessException("Data tidak ada","Data not found");
        }
        return employeeRepository.findById(id);
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    @Override
    public Iterable<Employee> findAll() {
        var empSave = employeeRepository.findAll();
        log.info("user save {} ", empSave);
        return empSave;
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Employee update(String id, int noRekening) {
        var empFind = employeeRepository.findById(id)
                .stream()
                .peek(employee -> employee.setNoRekening(employee.getNoRekening() + noRekening))
                .findFirst().orElseThrow();
        Employee employeeSave = employeeRepository.save(empFind);
        System.out.println(employeeSave);
        return employeeSave;
    }

    @Override
    public void delete(String id) {
        employeeRepository.deleteById(id);
    }
}
