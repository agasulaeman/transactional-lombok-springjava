package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.service;

import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity.Employee;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity.PayrollEmployee;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.exception.BusinessException;

public interface PayrollEmployeeService {
    public PayrollEmployee save (Employee employee, int noRekening) throws BusinessException;
}
