package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.dto;

import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity.Employee;
import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity.PayrollEmployee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataEmployeePayrollRequest {

    private PayrollEmployee payrollEmployee;

    private Employee employee;
}
