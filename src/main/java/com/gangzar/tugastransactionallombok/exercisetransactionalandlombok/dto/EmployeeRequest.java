package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Data
@Getter
@Setter

public class EmployeeRequest {

    private String id;
    private String nama;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date tanggalLahir;
    private double salary;
    private int noRekening;
    private String bankAccount;
    private String education;
}
