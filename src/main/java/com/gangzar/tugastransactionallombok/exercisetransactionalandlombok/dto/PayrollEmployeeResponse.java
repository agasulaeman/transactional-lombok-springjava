package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class PayrollEmployeeResponse {
    private String employeeUuid;
    //private double salary;
    private int noRekening;
   // private String bankAccount;
}
