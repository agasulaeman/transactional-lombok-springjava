package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.repository;

import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity.PayrollEmployee;
import org.springframework.data.repository.CrudRepository;

public interface PayrollEmployeeRepository extends CrudRepository<PayrollEmployee,String> {
}
