package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.repository;

import com.gangzar.tugastransactionallombok.exercisetransactionalandlombok.entity.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee,String> {
}
