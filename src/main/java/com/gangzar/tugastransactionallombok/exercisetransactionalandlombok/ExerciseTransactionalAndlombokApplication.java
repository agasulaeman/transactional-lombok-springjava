package com.gangzar.tugastransactionallombok.exercisetransactionalandlombok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExerciseTransactionalAndlombokApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExerciseTransactionalAndlombokApplication.class, args);
	}

}
